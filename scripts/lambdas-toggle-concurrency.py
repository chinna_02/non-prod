#!/usr/bin/env python3

import boto3
import csv
import argparse
import time

example_text = '''examples:
./lambda-toggle-concurrency.py --profile pokemon -r us-west-2 --csv ./lambdas.csv --concurrency toggle_on

./lambda-toggle-concurrency.py --profile pokemon -r us-west-2 --csv ./lambdas.csv --concurrency toggle_off
 '''

sleep_amount=30 # Sleep between deployments in seconds
deployment_limit=250 # Maximum amount of concurrency to deploy

parser = argparse.ArgumentParser(description='Process lambda provisioned concurrency',
                                epilog=example_text,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
required = parser.add_argument_group('Required arguments')
#required.add_argument('-p','--profile', action="store", dest="profile", help="AWS Credentials Profile to use", required=True)
required.add_argument('-r','--region', action="store", dest="region", help="AWS Region to perform actions against", required=True)
required.add_argument('--csv', action="store", dest="csv", help="CSV File to read Lambda Functions from", required=True)
required.add_argument('--concurrency', action="store", dest="concurrency",choices=['toggle_on', 'toggle_off'], help="Toggle Provisioned Concurrency On", required=True)
args = parser.parse_args()

#session = boto3.Session(profile_name=args.profile, region_name=args.region)
session = boto3.Session(region_name=args.region)
lam_client = session.client('lambda')


with open(args.csv, 'r', newline='', encoding='utf-8-sig') as csvfile:
    reader = csv.DictReader(csvfile)
    for func in reader:
        if args.concurrency == 'toggle_on' and func['alias'] and func['concurrency'] and int(func['concurrency']) > 0:
            func_concurrency = int(func['concurrency'])
            deployment_amount = deployment_limit if func_concurrency>deployment_limit else func_concurrency
            print("set provisioned conncurrency on")
            print("{} - {} - {} of {} ".format(func['function_name'], func['alias'], deployment_amount, func_concurrency ))

            lam_client.put_provisioned_concurrency_config(FunctionName=func['function_name'],
                                                          Qualifier=func['alias'],
                                                          ProvisionedConcurrentExecutions=deployment_amount)
            while deployment_amount != func_concurrency:
                print("sleeping {} seconds before next increase in concurrency".format(sleep_amount))
                time.sleep(sleep_amount)
                deployment_amount += deployment_limit if (func_concurrency-deployment_amount) > deployment_limit else (func_concurrency-deployment_amount)
                print("{} - {} - {} of {} ".format(func['function_name'], func['alias'], deployment_amount, func_concurrency ))
                lam_client.put_provisioned_concurrency_config(FunctionName=func['function_name'],
                                                              Qualifier=func['alias'],
                                                              ProvisionedConcurrentExecutions=deployment_amount)

        elif args.concurrency == 'toggle_off' and func['alias']:
            print("set provisioned conncurrency off")
            print("{} - {} - {} ".format(func['function_name'], func['alias'], func['concurrency'] ))
            lam_client.delete_provisioned_concurrency_config(FunctionName=func['function_name'],
                                                           Qualifier=func['alias'])
        elif func['reserved_concurrency'] and int(func['reserved_concurrency']) > 0:
            '''This will only run in "toggle_on" mode'''
            print("Set reserved concurrency")
            print("{} - {}".format(func['function_name'], int(func['reserved_concurrency'])))
            lam_client.put_function_concurrency(FunctionName=func['function_name'],
                                                  ReservedConcurrentExecutions=int(func['reserved_concurrency']))
