# tpci-aws-non-prod

This repository contains the configurations for non-production (non-prod) TPCi AWS account. It uses CodeBuild to apply these configurations as described below.

## Table of Contents

1. [Create initial build of configuration](#create-initial-build-of-configuration)
2. [config.json](#config.json)
3. [feature-toggles.json](#feature-toggles.json)
4. [Credentials](#credentials)
5. [pub-keys](#pub-keys)
6. [Add someone to git-secret repository](#add-someone-to-git-secret-repository)
7. [Deprecated v1 Format and Scripts](#deprecated-v1-format)
8. [References](#references)

## Create initial build of configuration

You will need appropriate AWS credentials to provide the following environment variables:

```bash
AWS_DEFAULT_REGION=us-west-2  # or appropriate region
AWS_ACCESS_KEY_ID=<...>
AWS_SECRET_ACCESS_KEY=<...>
AWS_SESSION_TOKEN=<...>
```

Clone the tpci-cloudformation-templates repo and build docker image

```bash
git clone git@bitbucket.org:slalomtpci/tpci-cloudformation-templates.git
cd tpci-cloudformation-templates
docker build -t tpci/tpci-cloudformation-cicd .
```

Then run the following command:

```bash
export REPO_CONFIG_JSON='{"name":"tpci-aws-non-prod","cicd-image":"053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-cloudformation-cicd:2.0.0","override-parameters":{"BuildEnvironmentsImage": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-build-images-dind:0.0.1","BaseEnvConnectivityTests": false, "PromoteEnvConnectivityTests": false,"ReleaseBuild": false}}'
docker run -e AWS_DEFAULT_REGION -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_SESSION_TOKEN \
  tpci/tpci-cloudformation-cicd create-sam-stack.sh --repo-config $REPO_CONFIG_JSON --stage dev
```

This will create a CodeBuild project for this repository. You may need to start the initial build which will in turn process the config.json in this repo.

## config.json

This file is used to configure Serverless Application Model (SAM) and Cloudformation stacks deployed to AWS.

### repos

Repos is a list containing git repositories that are deployed to AWS with in a region. The add the repository configuration to multiple region lists to make it available to deploy in multiple regions. Most repositories contain a Serverless Application Model template. For each repository a cicd stack is created containing 1 or more CodeBuild projects and (usually) a Codepipeline for deploying/testing/promoting the repository.

#### **Note**

The config.json is in a transitional state. The below information is only valid for repos that use a cicd image version of 2.0 or Greater. If a repo uses an older cicd-image, and the resulting older CI/CD pipeline template, the template process will fall back to reading the old format from `old.<config>.json`. For information on the prior format, skip to the [Deprecated v1 Format and Scripts](#deprecated-v1-format) section below.

A repo entry contains the following elements:

- **name**: Name of the git repository
- **cicd-image**: The full image path for the version of cicd image used to create/update pipeline
- **owner**: _OPTIONAL - Default value (slalomtpci)_. Specify an owner
- **organization**: _OPTIONAL - Default value (tpci)_. Specify an organization
- **project**: _OPTIONAL - Default value (cela)_. Specify a project
- **template**: _OPTIONAL - Default value (sam-bitbucket-template.yml)_. Specify a cicd template. Currently only 1 available.
- **override-parameters**: _OPTIONAL_ - Override the default values of any of the Cloudformation template input parameters.
  This is a json object of key value pairs. Use the name of the Cloudformation parameter for the key and then your desired value. Below is a list of available Override Parameters as of 8/3/2021
  - _RepositoryBranch: Default value is (master)_. This sets which Git branch to use as the basis for the pipeline. This is only used if a "Version" is not passed along as well, which is used for Prod environments where a specific Git Tagged version is used.
  - _DomainPrefix: Default value is ("")_. This sets the prefix on the custom domain for APIGW as a Cloudformation template parameter passed into the contained Cloudformation Deploy steps. Currently no repo uses this.
  - _BaseDomain: Default value is (pokemoncenter.com)_. This sets the custom domain to map an APIGW to. This is a Cloudformation template parameter passed into the contained Cloudformation Deploy steps
  - _RefsType: Default value is (heads)_. This sets up Codebuild to either check Git branchs(default) or Git Tags(Production).
  - _Version: Default value is ("")_. This is to set the default Git tag version number to trigger a build from, instead of a branch. Only used in production.
  - _ApprovalNotificationTopic: Default value is (tpci-approval-notification)_. This is the SNS topic to send Approval step notifications to.
  - _BaseEnv: Default value is (dev)_. This sets the first environment to deploy into.
  - _PromotionEnv: Default value is (stage)_. This sets the second environment to deploy into if tests pass, if applicable.
  - _StandardReferenceImage: Default value is (aws/codebuild/standard:5.0)_. This is the base utility docker image, used for Git commands currently.
  - _BuildEnvironmentImage: Default value is (aws/codebuild/standard:5.0)_. This is the docker image used for "Build" steps.
  - _ConnectivityTestImage: Default value is (aws/codebuild/standard:5.0)_. This is the docker image used for "Connectivity Test" steps.
  - _ReleaseImage: Default value is (aws/codebuild/standard:5.0)_. This is the docker image used for "Release" steps.
  - _BaseEnvDeploy: Default value is (true)_. This is a flag to determine if an explicit deploy step is needed. Some repos perform the deploy as part of the build step.
  - _BaseEnvConnectivityTests: Default value is (true)_. This is a flag to determine if it is appropriate to run Connectivity Test in the Base Env for a repo.
  - _PromoteApproval: Default value is (false)_. This is a flag to determine if an explicit approval step is required between the BaseEnv and the PromoteEnv.
  - _PromoteBuild: Default value is (false)_. This is a flag to determine if an explicit build step is required in the PromoteEnv.
  - _PromoteEnvDeploy: Default value is (true)_. This is a flag to determine if an explicit deploy step is required in the PromoteEnv.
  - _PromoteEnvConnectivityTests: Default value is (true)_. This is a flag to determine if it is appropriate to run Connectivity Test in the Promote Env for a repo.
  - _ReleaseBuild: Default value is (true)_. This is a flag to determine if it is appropriate to run a Release(git tag) after a passing the PromoteEnv for a repo.
  - _ExtraPermsCodeBuildPermsOrderStatus: Default value is (false)_. This is a flag for adding extra DynamoDB perms to the CodeBuild/Codepipeline IAM Role related to the AX Order Items Dynamodb table. Only used by tpci-order-status currently.
  - _ExtraPermsCodeBuildPermsCDK: Default value is (false)_. This is a flag for adding all extra IAM perms to the CodeBuild/Codepipeline IAM Role for an account. Only used by tpci-aws-cdk currently, so CDK can deploy a wide variety of resources.

Basic Example:

```json
"repos": {
    "us-west-2": [
      {
        "name": "tpci-aws-sam-ref-api",
        "cicd-image": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-cloudformation-cicd:2.0.0",
        "override-parameters": {
          "PromoteBuild": true
        }
      }
    ]
}
```

Customized Example

```json
"repos": {
    "us-west-2": [
      {
        "name": "tpci-aws-non-prod-test",
        "owner": "TPCI-IT",
        "project": "charmander",
        "cicd-image": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-cloudformation-cicd:2.0.10",
        "override-parameters": {
          "BuildEnvironmentsImage": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-build-images-dind:0.0.1",
          "BaseEnvConnectivityTests": false,
          "PromoteEnvConnectivityTests": false,
          "ReleaseBuild": false
        }
      },
    ],
    "eu-west-2": [
      {
        "name": "tpci-aws-non-prod-test",
        "owner": "TPCI-IT",
        "project": "charmander-eu",
        "cicd-image": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-cloudformation-cicd:2.0.10",
        "override-parameters": {
          "BuildEnvironmentsImage": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-build-images-dind:0.0.1",
          "ConnectivityTestImage": "053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-eu-test-image:0.0.1",
          "BaseEnvConnectivityTests": false,
          "PromoteEnvConnectivityTests": true,
          "ReleaseBuild": true
        }
      },
    ]
}
```

#### How to Update a repo

Add or update a repo entry in `config.json`. Create a new pull-request with the changes. Once the pull-request is merged the changes will applied. Check CodeBuild `tpci-aws-non-prod` for issues.

#### Deleting repo Entries

Deleting an entry will not remove the resources created. Currently stacks created must be manually deleted if an entry is removed.

## feature-toggles.json

This file is used to configure feature toggles. The values will be added/updated in AWS Parameter Store.
The below example json illustrates an entry in the file.

```json
{
  "featureName": "some-feature",
  "repository": "tpci-repository-name",
  "description": "Describe feature toggle",
  "versionAdded": "x.y.z",
  "values": {
    "dev": "<true/false>",
    "stage": "<true/false>"
  }
}
```

This will create string values in AWS Parameter Store with name

```json
/dev/feature-toggle/tpci-repository-name/some-feature
/stage/feature-toggle/tpci-repository-name/some-feature
```

And value `true` or `false` with description.

Configure `versionAdded` with the version of the repository when the feature toggle was added. This helps document the age of the feature toggle and determining when to decommission it.

You can optionally configure a `region` value in the feature toggle entry. This will override the default region value.

## Credentials

To add or update credentials first install `git-secret`

<https://git-secret.io/installation>

Credentials for 3rd party integrations are managed in the `assets` folder. Each credential is encrypted using git-secrets. Use `create-update-secrets.sh` to add or update a credential. This will create or update a secret in AWS secrets manager for the credential. You will need your gpg public key added to git-secrets to run the script and/or update the credential secret files. To add or update a credential follow these steps:

### Adding a credential

- Reveal secrets

```bash
git secret reveal
```

- Update `create-update-secrets.sh` with `createOrUpdateSecret` command for both `dev` and `stage`. The command takes three arguments, which are the credential name, description, and an optional "binary" flag.

```bash
# dev
createOrUpdateSecret "dev/xpo/status/credentials" "API key for XPO basic auth"

# stage
createOrUpdateSecret "stage/xpo/status/credentials" "API key for XPO basic auth"

# binary
createOrUpdateSecret "nonprod/git-secret/private-key" "GPG private key" binary
```

- Add the credential files to assets folder. The file name must be the same as the credential name with the backslash "/" replaced with ".". For the example above, the following two files would be added:

```bash
assets/dev.xpo.status.credentials
assets/stage.xpo.status.credentials
```

- Add the credentials to git-secret

```bash
git secret add assets/dev.xpo.status.credentials
git secret add assets/stage.xpo.status.credentials
```

- Hide the credential files. This will create new encrypted .secret files. These will be checked into git.

```bash
git secret hide
```

- Again make sure that the non-encrypted files are not added to stage and will not be committed. Commit `create-update-secrets.sh` and the .secret files and create a pull-request to add these files. Note, git-secret will update configurations in .gitsecret that also need committed.

```bash
git add create-update-secrets.sh
git add assets/dev.xpo.status.credentials.secret
git add assets/stage.xpo.status.credentials.secret

# add additional files that git-secret may have modified
```

- After the pull-request is reviewed and merged, check AWS Secrets Manager to confirm the new secret was added.

### Updating a credential

- Reveal secrets so that they can modified

```bash
git secret reveal
```

- Modify the secret in assets folder. If necessary, modify the entry in `create-update-secrets.sh` (e.g. changing description).

- Hide the secrets

```bash
git secret hide -d
```

- Create a new pull request. Ensure that no non-encrypted credentials are committed (only .secret). After the PR is reviewed and merged, check AWS Secrets Manager to confirm the new secret was updated.

For help using git-secret see <https://git-secret.io/>

## pub-keys

TPCi public key store for credentials. Add your public key to the pub-keys directory. Follow the steps below:

### Install gpg

MacOS

```bash
brew install gnupg
```

Windows

<https://www.gnupg.org/(en)/download/index.html>

### Generate keys

See [here](https://help.github.com/articles/generating-a-new-gpg-key/)

Select key type `RSA and RSA` for step 1 of `gpg --full-generate-key` when following the github documentation.
If you are an advanced user, select the apporpriate key type for you.

### Add pub key

Use `gpg --export` to export public key

Run command

```bash
gpg --export -a "First Last" > pub-keys/first.last.txt`
```

Should follow the format: `pub-keys/first.last.txt`

- Example: `pub-keys/travis.redfield.txt`

Create a pull-request. You will need to add someone who already has git-secret access.

## Add someone to git-secret repository

After user's public key has been added to pub-keys run the command:

```bash
./import-keys.sh
```

Now add the person to git-secrets using

```bash
git secret tell persons@email.id # this will be the email address associated with the public key
```

Re-encrypt files with new user

```bash
git secret reveal; git secret hide -d
```

Now create a pull request with the updated secret files and merge.

## References

See https://bitbucket.org/slalomtpci/tpci-cloudformation-templates/src/master/cicd/ for Cloudformation templates and scripts applied.

## Deprecated V1 Format - config.json

This is an archive of the now deprecated v1.X format for config.json. Once all repos are updated to the 2.X format, it should be possible to remove this and the "old.<config>.json" files.

### Create initial build of configuration - v1 compatible

You will need appropriate AWS credentials to provide the following environment variables:

```bash
AWS_DEFAULT_REGION=us-west-2  # or appropriate region
AWS_ACCESS_KEY_ID=<...>
AWS_SECRET_ACCESS_KEY=<...>
AWS_SESSION_TOKEN=<...>
```

Clone the tpci-cloudformation-templates repo and build docker image

```bash
git clone git@bitbucket.org:slalomtpci/tpci-cloudformation-templates.git
git checkout <A V1 format commit/branch/tag>
cd tpci-cloudformation-templates
docker build -t tpci/tpci-cloudformation-cicd .
```

Then run the following command:

```bash
docker run -e AWS_DEFAULT_REGION -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e AWS_SESSION_TOKEN \
  tpci/tpci-cloudformation-cicd create-sam-stack.sh \
    tpci/tpci-cloudformation-cicd create-sam-stack.sh \
    tpci-aws-non-prod \
    slalomtpci \
    master \
    dind \
    sam-bitbucket-template \
    tpci \
    cela \
    np- \
    NOT_DEFINED \
    dev \
    stage \
    heads \
    "" \
    pokemoncenter.com
```

### config.json for CICD-Image 1.X.X repos

Repos is a list containing git repositories that are deployed to AWS. Most repositories contain a Serverless Application Model template. For each repository a cicd stack is created containing 1 or more CodeBuild projects and (usually) a Codepipeline for deploying/testing/promoting the repository.

A repo entry contains the following elements:

- **name**: Name of the git repository
- **owner**: Owner of the git repository
- **branch**: Branch to be built
- **env**: CodeBuild environment. See `BuildEnvironment` parameter in <https://bitbucket.org/slalomtpci/tpci-cloudformation-templates/src/master/cicd/sam-bitbucket-template.yml> for more details
- **cicd-image**: Version of cicd image used to create/update pipeline
- **template**: Cloudformation template used to create/update CodeBuild and CodePipeline for repository
- **bucket-prefix**: A prefix for bucket name. To insure bucket names are unique.
- **domain-prefix**: A prefix for custom domain mapping. E.g. "pc.". Use NOT_DEFINED if no prefix required.
- **base-domain**: The base domain used for custom domain mapping. E.g. pokemoncenter.com

Example:

```json
{
  "name": "tpci-aws-sam-ref-api",
  "owner": "slalom-tpci",
  "branch": "master",
  "env": "nodejs8.11",
  "cicd-image": "0.0.1",
  "template": "sam-bitbucket-template",
  "bucket-prefix": "np-",
  "domain-prefix": "NOT_DEFINED",
  "base-domain": "pokemoncenter.com"
}
```

## TODO

- Add ability to mark repo/domain for deletion
