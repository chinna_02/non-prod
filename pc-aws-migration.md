# pokemoncenter AWS migration

## Steps/issues required to perform migration

- S3 buckets are global. As a result added `bucket-prefix` repo field so that buckets in pc account are uniquely named.
- Adding new (lower) sub-domains so that existing sub-domains are not affected. Adding lower domains:
  - pc.dev.api.slalom-dev.com
  - pc.dev.ui.slalom-dev.com
  - pc.stage.api.slalom-dev.com
  - pc.stage.ui.slalom-dev.com 
- Created hosted domains for sub-domains
  - pc.dev.ui.slalom-dev.com
    - in 175387783879 account created NS record for `pc.dev.ui.slalom-dev.com` on hosted zone `dev.ui.slalom-dev.com`
  - pc.dev.api.slalom-dev.com
    - in 175387783879 account created NS record for `pc.dev.api.slalom-dev.com` on hosted zone `dev.api.slalom-dev.com`
- Created hosted domains for pokemoncenter sub-domains
  - dev.api.pokemoncenter.com
  - dev.ui.pokemoncenter.com
  - stage.api.pokemoncenter.com
  - stage.ui.pokemoncenter.com
- Created new certificate in pokemon AWS. Certs cannot be used across accounts. As a result had to create cname records in hosted sub-domains in 175387783879 account
- Associating new certificate to existing domain required adding permission below on `*` resources
  - apigateway:POST
  - apigateway:GET
  - apigateway:DELETE
  - cloudfront:UpdateDistribution
  - route53:ListHostedZones
  - route53:ChangeResourceRecordSets
  - route53:GetChange
- Added following to parameter store (secure strings)
  - /webhook/cela_com_build_notify
  - /webhook/cela_oms_build_notify
- Created secrets in Secrets Manager
  - dev/xpo/credentials