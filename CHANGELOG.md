# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.9

- patch: cicd-image to 0.16.3

## 0.3.8

- patch: Revert old CICD image for Ecommweb-lambda
- patch: update CICD image for ecommweb-api stack

## 0.3.7

- patch: Update CICD Image for LAMBDA 

## 0.3.6

- patch: Update CICD Image for LAMBDA 

## 0.3.5

- patch: Update CICD Image for LAMBDA 

## 0.3.4

- patch: Update CICD Image for LAMBDA

## 0.3.3

- patch: Update CICD image for LAMBDA & Revert changes for CFT

## 0.3.2

- patch: update cicd image

## 0.3.1

- patch: update cicd image for ecommweb-lambda

## 0.3.0

- minor: update docker img version to 0.18.0

## 0.2.1

- patch: Add bloomreach cloud for deployments

## 0.2.0

- minor: Add feature toggles configuration

## 0.1.1

- patch: Add tpci-oms-plugins to config.json

## 0.1.0

- minor: Add tpci-aws-cdk
- minor: Remove custom domains as now managed by cdk
- minor: Update non-application repos to cicd image 0.15.0

## 0.0.5

- patch: Remove custom domain

## 0.0.4

- patch: Update tpci-order-status to fix DynamoDB permissions

## 0.0.3

- patch: Allow tpci-order-status to access DynamoDB

## 0.0.2

- patch: Add route53:ListResourceRecordSets to CodeBuildServiceRole

## 0.0.1

- patch: Add apigateway:PATCH to CodeBuildServiceRole
- patch: Update Bloomreach proxy credentials
- patch: Update cicd image to 0.11.3 to configure TLS 1.2 on domains
- patch: remove newline from br creds

