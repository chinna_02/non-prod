#!/usr/bin/env bash
# Conveinence script to import all gpg keys into your keychain

# shellcheck disable=SC2162
find pub-keys -type f |
while read f; do
  echo "Importing key ${f}"
  gpg --import "${f}"
done
