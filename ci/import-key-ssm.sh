#!/usr/bin/env bash

set -e

aws secretsmanager get-secret-value --secret-id nonprod/gitsecret/private-key | jq -r .SecretBinary | base64 -d > private.key

aws secretsmanager get-secret-value --secret-id nonprod/gitsecret/public-key | jq -r .SecretString > public.pgp

# Import private & public keys

gpg --import private.key
gpg --import public.pgp
