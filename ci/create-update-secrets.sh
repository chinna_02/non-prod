#!/usr/bin/env bash

###############################################################################
# create-update-secrets.sh
#
# Creates or updates the secrets in AWS secrets manager used for integrating
# with 3rd party sytems
#
# Must be ran with appropriate AWS_DEFAULT_PROFILE exported
#
# Usage:
#   create-update-secrets.sh
#
###############################################################################

export AWS_PAGER=

# We deploy the same secrets in all regions
# All the regions to deploy the secrets to
regions=("us-west-2" "eu-west-1")

function createOrUpdateSecret() {
  secretId=${1}
  description=${2}
  binary=${3}
  credentialsFile=$(echo "$secretId" | tr '/' '.')

  echo "secret: ${secretId}"
  echo "descripton: ${description}"
  echo "credentials file: ${credentialsFile}"

  if [[ $binary = "binary" ]]; then
    SECRET_PARAM=("--secret-binary" "fileb://assets/${credentialsFile}")
  else
    SECRET_PARAM=("--secret-string" "file://assets/${credentialsFile}")
  fi

  for region in "${regions[@]}";
    do

    if [[ $region == 'eu-west-1' ]]; then
      stage=$(echo "${secretId}" | cut -d '/' -f 1)
      secretName=$(echo "${secretId}" | cut -d '/' -f 2-)
      stage="${stage}-eu"
      secretId="${stage}/${secretName}"
    fi

    if aws --region "${region}" secretsmanager describe-secret --secret-id "${secretId}" > /dev/null 2>&1 ; then
      echo "secret ${secretId} exits, updating..."
      aws --region "${region}" secretsmanager update-secret --secret-id "${secretId}" --description "${description}" "${SECRET_PARAM[@]}"
    else
      echo "secret ${secretId} not found, creating..."
      aws --region "${region}" secretsmanager create-secret --name "${secretId}" --description "${description}" "${SECRET_PARAM[@]}"
    fi
  done
}

# unhide secrets
git secret reveal

createOrUpdateSecret "bitbucket/password" "BitBucket App Password for user TPCI-PCenter-Automation"

createOrUpdateSecret "nonprod/gitsecret/private-key" "Private key used to reveal git secrets from tpci-aws-non-prod during deployment" binary
createOrUpdateSecret "nonprod/gitsecret/public-key" "Public key used to encrypt git secrets from tpci-aws-non-prod during deployment"
createOrUpdateSecret "nonprod/brxm/aws-access-key-id" "AWS Access Key ID to allow Bloomreach XM to invalidate CloudFront cache"
createOrUpdateSecret "nonprod/brxm/aws-secret-access-key" "AWS Secret Access Key to allow Bloomreach XM to invalidate CloudFront cache"
createOrUpdateSecret "nonprod/dependentLibrary/credentials" "AWS credentials to allow builds in Bitbucket to pull from np-tpci-oms-utility-library-dev"

# dev
createOrUpdateSecret "dev/xpo/credentials" "Credentials to the XPO api, used to submit orders to XPO"
createOrUpdateSecret "dev/fluent/credentials" "Credentials to obtain oauth token from fluent to access their rest api"
createOrUpdateSecret "dev/fluent/deploy-credentials" "Fluent credentials for deploying tpci-oms-plugins to sandbox"
createOrUpdateSecret "dev/xpo/status/credentials" "Username/Password for basic auth, used by XPO when calling order status update"
createOrUpdateSecret "dev/fluent-webhook/api-key" "APIGW api-key to fluent-webhook api, used by test structure to execute contract tests"
createOrUpdateSecret "dev/bloomreach/search-login-credentials" "Login credentials for Bloomreach Search and Merch REST API"
createOrUpdateSecret "dev/bloomreach/maven" "Credentials to Bloomreach Maven repository"
createOrUpdateSecret "dev/bloomreach/cloud" "Credentials to Bloomreach Cloud https://missioncontrol-tpci1.onehippo.io/"
createOrUpdateSecret "dev/bloomreach/xm-basic-auth" "Login credentials for CloudFront to proxy to BloomReach"
createOrUpdateSecret "dev/bloomreach/sftp/credentials" "ep brsm plugin sftp private key"
createOrUpdateSecret "dev/transaction-email-api/fluent-webhook-client" "The dev client credentials for the Fluent webhook"
createOrUpdateSecret "dev/e-commerce/cybersource" "CyberSource API keys for US"
createOrUpdateSecret "dev/e-commerce/cybersource-ca" "CyberSource API keys for CA"
createOrUpdateSecret "dev/e-commerce/cybersource-uk" "CyberSource API keys for UK"
createOrUpdateSecret "dev/avalara/credentials" "Avalara Credentials for API Access"
createOrUpdateSecret "dev/new-relic/api-key/credentials" "API key for New Relic"
createOrUpdateSecret "dev/yotpo/api-key" "API key for Non-Prod Yotpo integration"
createOrUpdateSecret "dev/order-management/shared-library-credentials" "Credentials to download dependencies for OMS components"
createOrUpdateSecret "dev/fluent-cross-retailer/credentials" "Fluent credentials to obtain token for Fluent user with cross retailer permissions"
createOrUpdateSecret "dev/narvar/api-key" "API key for Non-Prod Narvar integration"
createOrUpdateSecret "dev/narvar/api-returns-key" "API key for Non-Prod Narvar Returns integration"
createOrUpdateSecret "dev/fluent-ep-service/credentials" "Fluent credentials for the EP service user"
createOrUpdateSecret "dev/fluentadmin/credentials" "dev Fluent admin credentials for api key rotation"
createOrUpdateSecret "dev/cela-136-test/credentials" "cela-136 testing"
createOrUpdateSecret "dev/lp/status/credentials" "Credentials for logistics provider to call our Order Status API."
createOrUpdateSecret "dev/flexe/token/credentials" "Token for Flexe API access"
createOrUpdateSecret "dev/flexe-access/credentials" "Creds to allow Flexe access to our API"
createOrUpdateSecret "dev/xpo/edi/credentials" "Creds to allow access to XPO's rest EDI API"
createOrUpdateSecret "dev/xpo/edi-return/credentials" "Creds to allow XPO to send us edi return info"
createOrUpdateSecret "dev/ingram/inbound/credentials" "Creds to allow IngramMicro to make calls to our APIs"
createOrUpdateSecret "dev/ingram/outbound/credentials" "Creds to call IngramMicro APIs to submit orders"

# stage
createOrUpdateSecret "stage/xpo/credentials" "Credentials to the XPO api, used to submit orders to XPO"
createOrUpdateSecret "stage/fluent/credentials" "Credentials to obtain oauth token from fluent to access their rest api"
createOrUpdateSecret "stage/fluent/deploy-credentials" "Fluent credentials for deploying tpci-oms-plugins to sandbox"
createOrUpdateSecret "stage/xpo/status/credentials" "Username/Password for basic auth, used by XPO when calling order status update"
createOrUpdateSecret "stage/fluent-webhook/api-key" "APIGW api-key to fluent-webhook api, used by test structure to execute contract tests"
createOrUpdateSecret "stage/bloomreach/search-login-credentials" "Login credentials for Bloomreach Search and Merch REST API"
createOrUpdateSecret "stage/bloomreach/maven" "Credentials to Bloomreach Maven repository"
createOrUpdateSecret "stage/bloomreach/cloud" "Credentials to Bloomreach Cloud https://missioncontrol-tpci1.onehippo.io/"
createOrUpdateSecret "stage/bloomreach/xm-basic-auth" "Login credentials for CloudFront to proxy to BloomReach"
createOrUpdateSecret "stage/bloomreach/sftp/credentials" "ep brsm plugin sftp private key"
createOrUpdateSecret "stage/transaction-email-api/fluent-webhook-client" "The stage client credentials for the Fluent webhook"
createOrUpdateSecret "stage/e-commerce/cybersource" "CyberSource API keys for US"
createOrUpdateSecret "stage/e-commerce/cybersource-ca" "CyberSource API keys for CA"
createOrUpdateSecret "stage/e-commerce/cybersource-uk" "CyberSource API keys for UK"
createOrUpdateSecret "stage/avalara/credentials" "Avalara Credentials for API Access"
createOrUpdateSecret "stage/new-relic/api-key/credentials" "API key for New Relic"
createOrUpdateSecret "stage/yotpo/api-key" "API key for Non-Prod Yotpo integration"
createOrUpdateSecret "stage/order-management/shared-library-credentials" "Credentials to download dependencies for OMS components"
createOrUpdateSecret "stage/fluent-cross-retailer/credentials" "Fluent credentials to obtain token for Fluent user with cross retailer permissions"
createOrUpdateSecret "stage/narvar/api-key" "API key for Non-Prod Narvar integration"
createOrUpdateSecret "stage/narvar/api-returns-key" "API key for Non-Prod Narvar Returns integration"
createOrUpdateSecret "stage/fluent-ep-service/credentials" "Fluent credentials for the EP service user"
createOrUpdateSecret "stage/fluentadmin/credentials" "stage Fluent admin credentials for api key rotation"
createOrUpdateSecret "stage/cela-136-test/credentials" "cela-136 testing"
createOrUpdateSecret "stage/lp/status/credentials" "Credentials for logistics provider to call our Order Status API."
createOrUpdateSecret "stage/flexe/token/credentials" "Token for Flexe API access"
createOrUpdateSecret "stage/flexe-access/credentials" "Creds to allow Flexe access to our API"
createOrUpdateSecret "stage/xpo/edi/credentials" "Creds to allow access to XPO's rest EDI API"
createOrUpdateSecret "stage/xpo/edi-return/credentials" "Creds to allow XPO to send us edi return info"
createOrUpdateSecret "stage/ingram/inbound/credentials" "Creds to allow IngramMicro to make calls to our APIs"
createOrUpdateSecret "stage/ingram/outbound/credentials" "Creds to call IngramMicro APIs to submit orders"

# hfe
createOrUpdateSecret "hfe/xpo/credentials" "Credentials to the XPO api, used to submit orders to XPO"
createOrUpdateSecret "hfe/fluent/credentials" "Credentials to obtain oauth token from fluent to access their rest api"
createOrUpdateSecret "hfe/xpo/status/credentials" "Username/Password for basic auth, used by XPO when calling order status update"
createOrUpdateSecret "hfe/fluent-webhook/api-key" "APIGW api-key to fluent-webhook api, used by test structure to execute contract tests"
createOrUpdateSecret "hfe/bloomreach/search-login-credentials" "Login credentials for Bloomreach Search and Merch REST API"
createOrUpdateSecret "hfe/bloomreach/cloud" "Credentials to Bloomreach Cloud https://missioncontrol-tpci1.onehippo.io/"
createOrUpdateSecret "hfe/bloomreach/xm-basic-auth" "Login credentials for CloudFront to proxy to BloomReach"
createOrUpdateSecret "hfe/bloomreach/sftp/credentials" "ep brsm plugin sftp private key"
createOrUpdateSecret "hfe/transaction-email-api/fluent-webhook-client" "The stage client credentials for the Fluent webhook"
createOrUpdateSecret "hfe/e-commerce/cybersource" "CyberSource API keys for US"
createOrUpdateSecret "hfe/e-commerce/cybersource-ca" "CyberSource API keys for CA"
createOrUpdateSecret "hfe/e-commerce/cybersource-uk" "CyberSource API keys for UK"
createOrUpdateSecret "hfe/avalara/credentials" "Avalara Credentials for API Access"
createOrUpdateSecret "hfe/new-relic/api-key/credentials" "API key for New Relic"
createOrUpdateSecret "hfe/yotpo/api-key" "API key for Non-Prod Yotpo integration"
createOrUpdateSecret "hfe/order-management/shared-library-credentials" "Credentials to download dependencies for OMS components"
createOrUpdateSecret "hfe/narvar/api-key" "API key for Non-Prod Narvar integration"
createOrUpdateSecret "hfe/lp/status/credentials" "Credentials for logistics provider to call our Order Status API."


# special "migrationtest" credential
createOrUpdateSecret "migration-test/fluent/credentials" "Credentials to obtain oauth token from fluent to access their rest api"

# hide secrets
git secret hide -d -F
