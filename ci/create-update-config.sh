#!/usr/bin/env bash

###############################################################################
# create-update-config.sh
#
# Creates or updates the build projects and resources configured in provided
# configuration file
#
# Usage:
#   create-update-config.sh <config.json> <stage> <promotion-stage>
#
###############################################################################

function oldStyleDeploy(){
  local deploy_region=$1
  local deploy_repo=$2
  local config_file="old.${config_file}"
  echo "$deploy_region $deploy_repo"

# for each repository configuration create/update the cicd stack
  jq --arg REPO "$deploy_repo" -r '.repos[] | select(.type == "sam-stack") | select(.params.name == $REPO) |.params | [
    .name,
    .owner,
    .branch,
    .env,
    .["cicd-image"],
    .template,
    .["bucket-prefix"],
    .["domain-prefix"],
    .["base-domain"],
    .version,
    .["refs-type"]] | @tsv' "${config_file}" |
  while IFS=$'\t' read -r name owner branch env image template bucketprefix subdomain basedomain version refsType; do
    echo
    echo "Recieved parameters...
      repository: ${name}
      owner: ${owner}
      branch: ${branch}
      env: ${env}
      image: ${image}
      template: ${template}
      bucket prefix: ${bucketprefix}
      domain prefix: ${subdomain}
      base domain: ${basedomain}
      refs type: ${refsType}
      version: ${version}"

    echo
    echo "Calling create-sam-stack.sh for repo: ${name}:${version}"
    echo "docker run \
      -e AWS_DEFAULT_REGION \
      -e AWS_CONTAINER_CREDENTIALS_RELATIVE_URI \
      053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-cloudformation-cicd:${image} create-sam-stack.sh \
      ${name} \
      ${owner} \
      ${branch} \
      ${env} \
      ${template} \
      tpci \
      cela \
      ${bucketprefix} \
      ${subdomain} \
      ${deploy_stage} \
      ${promotion_stage} \
      ${refsType:-heads} \
      ${version:-0.0.0} \
      ${basedomain}"

    docker run \
      -e AWS_DEFAULT_REGION \
      -e AWS_CONTAINER_CREDENTIALS_RELATIVE_URI \
      053655145506.dkr.ecr.us-west-2.amazonaws.com/tpci/tpci-cloudformation-cicd:"${image}" create-sam-stack.sh \
      "${name}" \
      "${owner}" \
      "${branch}" \
      "${env}" \
      "${template}" \
      tpci \
      cela \
      "${bucketprefix}" \
      "${subdomain}" \
      "${deploy_stage}" \
      "${promotion_stage}" \
      "${refsType:-heads}" \
      "${version:-0.0.0}" \
      "${basedomain}"
    return $?
  done
}

function newDeploy(){
  deploy_region=$1
  repo_config=$2
  cicd_image=$(echo "$repo_json" | jq -cr '.["cicd-image"]')

  echo "docker run -e $deploy_region \
       -e AWS_CONTAINER_CREDENTIALS_RELATIVE_URI \
        $cicd_image create-sam-stack.sh \
        -c $repo_config \
        -s $deploy_stage"

  docker run \
    -e "AWS_DEFAULT_REGION=$deploy_region" \
    -e AWS_CONTAINER_CREDENTIALS_RELATIVE_URI \
    "$cicd_image" create-sam-stack.sh -c "$repo_config" -s "$deploy_stage" -b "np-"
  return $?
}

echo "dev hfe" | grep -qw "$1" || {
  echo "STAGE should be one of dev|hfe"
  exit 1
}

echo "Running AWS-NONPROD for $1"

if [[ $1 == "dev" ]]
then
  config_file="config.json"
  deploy_stage="dev"
  promotion_stage="stage"
else
  config_file="hfe-config.json"
  deploy_stage="hfe"
  promotion_stage="-"
fi

if [[ ! -f "$config_file" ]];
then
  echo "Config file: $config_file not found in running directory."
  exit 2
fi


echo "Configuring deployments in ${config_file} for stage \"${deploy_stage}\" and promotion stage \"${promotion_stage}\""
echo

failures=()
mapfile -t regions_arr < <(jq -r '.repos | keys[]' config.json)
for region in "${regions_arr[@]}"
do
repos=$(jq --arg REGION "$region" -cr '.repos | with_entries(select([.key] | inside([$REGION])))[][] ' config.json)
  for repo_json in ${repos[$@]}
  do
    repo_name=$(echo "$repo_json" | jq -cr '.name')
    cicd_template_version=$(echo "$repo_json" | jq -cr '.["cicd-image"]' | cut -d ':' -f 2 | cut -d '.' -f 1 )
    if (( "$cicd_template_version" < "2" ))
    then
      echo "$repo_name cicd_image is lower than 2.0.0"
      echo -e "Using old deployment style. Consider upgrading\n"

      oldStyleDeploy "$region" "$repo_name"
      rc=$?
      if [ "$rc" -gt 0 ]
      then
        failures+=("$region-$repo_name: Docker exit code $rc")
      fi
    else
      newDeploy "$region" "$repo_json"
      rc=$?
      if [ "$rc" -gt 0 ]
      then
        failures+=("$region-$repo_name: Docker exit code $rc")
      fi
    fi
  done
done

if [ ${#failures[@]} -gt 0 ]
then
  echo "Failed deployments were"
  for failure in "${failures[@]}"
  do
    echo "$failure"
  done
  exit 1
fi
exit 0
