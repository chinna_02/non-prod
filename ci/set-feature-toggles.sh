#!/usr/bin/env bash

set -e

###############################################################################
# set-feature-toggles.sh
#
# Updates feature toggles in AWS Parameter Store
#
# Usage:
#   set-feature-toggles.sh <feature-toggles.json>
#
###############################################################################


config_file=${1}

function updateToggle() {
  featureKey=${1}
  value=${2}
  description=${3}
  region=${4:-$AWS_REGION}

  echo "Checking feature toggle ${featureKey} in ${region}"
  old=$(aws --region "${region}" ssm get-parameter --name "${featureKey}" 2>/dev/null | jq -r '.Parameter.Value')
  echo "Current feature toggle value: ${old}"
  if [[ "$old" != "$value" ]]; then
    echo "Updating feature toggle ${featureKey} in ${region} with value ${value}"
    aws --region "${region}" ssm put-parameter --name "${featureKey}" --value "${value}" --description "${description}" --type String --overwrite
  fi
}

echo "Updating feature toggles..."

jq -r '.featureToggles[] | [
    .featureName,
    .repository,
    .description,
    .region ] | @tsv' "${config_file}" |
  while IFS=$'\t' read -r featureName repository description region; do
    echo "Recieved feature toggle...
      featureName: ${featureName}
      repository: ${repository}
      description: ${description}
      region: ${region}"

    jq -r --arg featureName "$featureName" '.featureToggles[] | select(.featureName==$featureName)
      | .values | to_entries | .[] | [.key, .value] | @tsv' "${config_file}" |
    while IFS=$'\t' read -r stage value ; do
      featureKey="/${stage}/feature-toggle/${repository}/${featureName}"
      updateToggle "${featureKey}" "${value}" "${description}" "${region}"
    done
  done
